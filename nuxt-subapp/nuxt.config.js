import { APP_CONSTANTS } from './constants/'
import i18n from './plugins/i18n'
const StatsPlugin = require('stats-webpack-plugin')
const path = require('path')

export default {
  // If we have server middleware, we need to call it using absolute paths instead of relative paths,
  // since this application might be running under a different root project.
  // So calling an endpoint that used to be /exampleEndpoint should be changed to process.env.baseURL/exampleEndpoint,
  // otherwise the endpoint might resolve to the root application.
  telemetry: { enabled: true, consent: true },
  publicRuntimeConfig: {
    appName: APP_CONSTANTS.APP_NAME,
    appVersion: APP_CONSTANTS.APP_VERSION,
    appRoleAdmin: APP_CONSTANTS.APP_ROLE_ADMIN,
    appHomeUrl: APP_CONSTANTS.APP_HOME
  },
  // router: {
  //   middleware: ['navigation-guard-admin']
  // },
  ssr: false,
  server: {
    port: APP_CONSTANTS.APP_PORT // default: 3000
  },
  // env: {
  //   baseURL: `${process.env.PROTOCOL}://${process.env.HOST}:${process.env.PORT}`
  // },
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'spa',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: APP_CONSTANTS.APP_NAME || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: APP_CONSTANTS.APP_DESCRIPTION || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [{ src: '~/assets/scss/main.scss', lang: 'scss' }],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~/plugins/global-error-handler.js', mode: 'client' },
    { src: '~/plugins/premium-tesh-ui-lib.js', mode: 'client' },
    { src: '~/plugins/after-each.js', mode: 'client' },
    { src: '~/plugins/axios.js', mode: 'client' }
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: ['~/components'],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/composition-api/module',
    '@nuxtjs/device'
    // [
    //   '@nuxtjs/router',
    //   //
    //   { keepDefaultRouter: true, path: './router', fileName: 'index.js' },
    // ],
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@femessage/nuxt-micro-frontend',
    // https://go.nuxtjs.dev/buefy
    ['nuxt-buefy', { materialDesignIcons: false }],
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      '@nuxtjs/i18n',
      {
        defaultLocale: 'es',
        locales: ['es', 'en'],
        strategy: 'no_prefix',
        vueI18n: i18n,
        vueI18nLoader: true,
        detectBrowserLanguage: false,
        parsePages: false,
        silentTranslationWarn: true,
        silentFallbackWarn: true
      }
    ],
      //  { handler: require(path.resolve(__dirname, '../../../')) }
  ],
  axios: {
    proxy: true // Can be also an object with default options
  },
  proxy: {
    '/api-data/': 'https://krystalnews.premiumtesh.nat.cu'
  },
  pwa: {
    meta: {
      title: APP_CONSTANTS.APP_NAME,
      author: 'PremiumTesh'
    },
    manifest: {
      name: APP_CONSTANTS.APP_NAME_EXTENDED,
      short_name: APP_CONSTANTS.APP_NAME,
      lang: 'en',
      display: 'standalone'
    }
  },

  MFE: {
    force: true,
    output: {
        library: `nuxt-subapp-app`,
        libraryTarget: 'window',
    }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    // Make sure all generated bundles and chunks point to your server.
    // If running in production meaning that the application will be built using npm run build && npm run start, point host to your domain.
    // This is done in package.json.
    // extractCSS: true,
    // publicPath: `//${process.env.HOST}:${process.env.PORT}/_nuxt/`,
    extend(config) {
      config.plugins.push(new StatsPlugin('manifest.json', {
            chunkModules: false,
            entrypoints: true,
            source: false,
            chunks: false,
            modules: false,
            assets: false,
            children: false,
            exclude: [/node_modules/]
        }))
    }
  }
}
