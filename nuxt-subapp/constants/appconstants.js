export const APP_CONSTANTS = {
  APP_NAME: 'Krystal News',
  APP_VERSION: 'v1.0.0',
  APP_PORT: 4200,
  APP_DESCRIPTION: 'Krystal News System',
  APP_NAME_EXTENDED: 'Krystal News',
  APP_HOME: '/rundown',
  APP_CLIENT_ID: 'NewsSystemFrontend',
  APP_SCOPE: 'NewsSystemApi',
  APP_ROLE_ADMIN: 'Admin',
  APP_NAME_FOR_IDENTITY: 'NewsSystem'
}
