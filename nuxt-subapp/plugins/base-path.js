export default async ({ app }) => {
  try {
    const data = await app.$axios.get(`${window.location.origin}/config.json`)
    // Set axios configurations
    // app.$axios.setBaseURL(`${data.data.API_DATA_PREFIX}`)
    // app.$axios.setHeader('Access-Control-Allow-Origin', '*')

    // Store Api constants
    app.store.commit('defineApiUrl', data.data.APP_BASE_URL)
    app.store.commit('defineApiPrefix', data.data.API_DATA_PREFIX)
    app.store.commit('defineLogoUrl', data.data.LOGO_PREFIX)
    app.store.commit('defineIdentityApiUrl', data.data.API_IDENTITY_PREFIX)
    app.store.commit('defineServerDateFormat', data.data.SERVER_DATE_FORMAT)
    app.store.commit('defineAppHealthUrl', data.data.APP_HEALTH_URL)
    app.store.commit(
      'defineAppUser',
      app.$auth.$state.user
        ? app.$auth.$state.user.name || app.$auth.$state.user.userName
        : null
    )
    app.store.commit(
      'defineAppUserEmail',
      app.$auth.$state.user ? app.$auth.$state.user.email : null
    )
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e.message)
  }
}
